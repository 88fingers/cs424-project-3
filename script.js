// Programmer: Laith Sarhan and Sneha Tuteja
// Assignment:  CS424 Project 3
// Date:        12/09/2016
// Description: This is the main script file that handles all google map api calls
//              and map functions

window.initMap = function() {
   document.getElementById("radio1998").checked=true;
   document.getElementById("defaultOpen").clicked = true;

  var el = document.querySelector('#map');
  var google = window.google;
  var lakeview = {lat: 41.9415, lng: -87.6622};
  var bronzeville = {lat: 41.8320, lng: -87.6259};
  var westloop = {lat: 41.8825, lng: -87.6447};
  var museum = {lat:41.92658268256411, lng: -87.63517603006787 }
  var fh = {lat:41.79195015804045, lng: -87.57981466307524 }


  // Create the Google Map…
  var map = new google.maps.Map(d3.select("#map").node(), {
    zoom: 11,
    center: new google.maps.LatLng(41.88205, -87.627815),
    mapTypeId: google.maps.MapTypeId.TERRAIN,
      styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
            ]
  });



  map.data.loadGeoJson('chicago_communities.geojson');

  map.data.setStyle(function(feature) {
          var color = 'gray';
          if (feature.getProperty('isColorful')) {
            color = feature.getProperty('color');
          }
          return /** @type {google.maps.Data.StyleOptions} */({
            fillColor: color,
            strokeColor: color,
            strokeWeight: 1
          });
        });

        map.data.addListener('click', function(event) {



         });


        map.data.addListener('mouseover', function(event) {
          map.data.revertStyle();
          map.data.overrideStyle(event.feature, {fillColor: 'red',
          strokeWeight: 2});

        });

        map.data.addListener('mouseout', function(event) {
          map.data.revertStyle();

        });

var iconmap1 ={
    url: 'marker.png',
    scaledSize: new google.maps.Size(50,50)

  };

  var iconmuseum ={
    url: 'museum.png',
    scaledSize: new google.maps.Size(50,50)

  }

  var iconfh ={
    url: 'field.png',
    scaledSize: new google.maps.Size(50,50)

  }

 var markerlakeview = new google.maps.Marker({
          position: lakeview,
          map: map,
          icon: iconmap1,
          title: 'Lake View Click to see images'
        });
 var markermuseum = new google.maps.Marker({
          position: museum,
          map: map,
          icon: iconmuseum,
          scaledSize: new google.maps.Size(10,10)
         });

var markerfh = new google.maps.Marker({
          position: fh,
          map: map,
          icon: iconfh,
          scaledSize: new google.maps.Size(10,10)
         });


var markerbronzeville = new google.maps.Marker({
          position: bronzeville,
          map: map,
          icon: iconmap1,
          title: 'Bronzeville Click to see images'
        });

var markerwestloop = new google.maps.Marker({
          position: westloop,
          map: map,
          icon: iconmap1,
          title: 'West Loop Click to see images'
        });

var infowindowlakeview = new google.maps.InfoWindow({
          maxWidth: 200,
          maxHeight: 200,
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1998lakeviewicon.jpg">'
           + '</br>' + '<button id="myBtn" onclick="imagefunctionlakeview()">See more images</button>'
        });

var infowindowwestloop = new google.maps.InfoWindow({
    maxWidth: 200,
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1998westloopicon.jpg">' + '</br>'
           + '<button id="myBtn" onclick="imagefunctionwestloop()">See more images</button>'
        });

var infowindowbronzeville= new google.maps.InfoWindow({
          maxWidth: 200,
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1998bronzevilleicon.jpg">' + '</br>'
           + '<button id="myBtn" onclick="imagefunctionbronzeville()">See more images</button>'
        });

 var infowindowmuseum = new google.maps.InfoWindow({
          content: 'Built in 1999' + '</br>' + 'Peggy Notebaert Nature Museum'
        })

var infowindowfh = new google.maps.InfoWindow({
          content: 'Built in 1998' + '</br>' + '57th Street Beach House'
        })

markerlakeview.addListener('click', function() {
        //  map.setZoom(13);
        //  map.setCenter(markerlakeview.getPosition());
          infowindowlakeview.open(map, markerlakeview);
        });

markermuseum.addListener('click', function() {
        //  map.setZoom(13);
        //  map.setCenter(markerlakeview.getPosition());
          infowindowmuseum.open(map, markermuseum);
        });

markerfh.addListener('click', function() {
        //  map.setZoom(13);
        //  map.setCenter(markerlakeview.getPosition());
          infowindowfh.open(map, markerfh);
        });

markerwestloop.addListener('click', function() {
        //  map.setZoom(13);
        //  map.setCenter(markerwestloop.getPosition());
          infowindowwestloop.open(map, markerwestloop);
        });

markerbronzeville.addListener('click', function() {
        //  map.setZoom(13);
        //  map.setCenter(markerbronzeville.getPosition());
          infowindowbronzeville.open(map, markerbronzeville);
        });

var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
        //  map.fitBounds(bounds);
        });
var icons = {
          parking: {
            name: 'Field House',
            icon: 'field.png',
            scaledSize: new google.maps.Size(10,10)
          },
          library: {
            name: 'Pool Building',
            icon: 'water.png',
            scaledSize: new google.maps.Size(10,10)
          },
          museum: {
            name: 'Museum',
            icon: 'museum.png',
            scaledSize: new google.maps.Size(10,10)
          },
          info: {
            name: 'Golf Building',
            icon: 'golf.ico',
            scaledSize: new google.maps.Size(10,10)

          }
        };
var legend = document.getElementById('legend');
        for (var key in icons) {
          var type = icons[key];
          var name = type.name;
          var icon = type.icon;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + icon + '"> ' + name;
          legend.appendChild(div);
        }

        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);

};
var svgnew= d3.select("body").append("svg").attr("height",35).attr("width",500).attr("id","togglesvg");

var toggle = 0;
/*var togglerect = svgnew.append("rect").attr("width",150)
                                    .attr("height",50)
                                    .attr("x",0)
                                    .attr("y",10)
                                    .attr("fill","black")
                                    .on("click", myFunction);

svgnew.append("text").attr("font-size",20)
                                    .attr("fill","white")
                                    .attr("x",5)
                                    .attr("y",30)
                                    .text("Click here")
                                    .on("click", myFunction);
*/

function imagefunctionlakeview(){

  var modal = document.getElementById('myModallakeview');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var img1 = document.getElementById('myImg1');
var img2 = document.getElementById('myImg2');
var img3 = document.getElementById('myImg3');
var img4 = document.getElementById('myImg4');
var img5 = document.getElementById('myImg5');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

img.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img1.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img2.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img3.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img4.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img5.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}

function imagefunctionbronzeville(){

  var modal = document.getElementById('myModalbronzeville');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var imgbronze = document.getElementById('myImgbronze');
var img1bronze = document.getElementById('myImg1bronze');
var img2bronze = document.getElementById('myImg2bronze');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

imgbronze.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img1bronze.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img2bronze.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}

function imagefunctionwestloop(){

  var modal = document.getElementById('myModalwestloop');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var imgLOOP = document.getElementById('myImgLOOP');
var img1LOOP = document.getElementById('myImg1LOOP');
var img2LOOP = document.getElementById('myImg2LOOP');
var img3LOOP = document.getElementById('myImg3LOOP');
var img4LOOP = document.getElementById('myImg4LOOP');
var img5LOOP = document.getElementById('myImg5LOOP');
var img6LOOP = document.getElementById('myImg6LOOP');
var img7LOOP = document.getElementById('myImg7LOOP');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

imgLOOP.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img1LOOP.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img2LOOP.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img3LOOP.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img4LOOP.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img5LOOP.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img6LOOP.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img7LOOP.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}


function myFunction(){
if (toggle == 0){
  toggle = 1;
  d3.select('#grid').style("visibility", "visible");
console.log("button clicked");
var square = 20,
  w = 900,
  h = 300;

// create the svg
var svg = d3.select('#grid').append('svg')
  .attr({
    width: w,
    height: h
  })
  .attr("id","sqauresvg");

d3.select('#grid').append('svg')
.attr("width", 200)
.attr("height",300)
.attr("id","imagesvg");

// calculate number of rows and columns
var squaresRow = Math.round(w / square);
var squaresColumn = Math.round(h / square);

// loop over number of columns
 for(var n = 0; n < squaresColumn; n++){

  // create each set of rows
  var rows = svg.selectAll('rect' + ' .row-' + (n + 1))
    .data(d3.range(squaresRow))
    .enter().append('rect')
    .attr({
      class: function(d, i) {
        return 'square row-' + (n + 1) + ' ' + 'col-' + (i + 1);
      },
      id: function(d, i) {
        return 's-' + (n + 1) + (i + 1);
      },
      width: square,
      height: square,
      x: function(d, i) {
        return i * square;
      },
      y: n * square,
      fill: '#fff',
      stroke: '#FDBB30',
      opacity:'0.3'
    });

    // test with some feedback
    var test = rows.on('mouseover', function (d, i) {
      d3.select('#grid-ref').text(function () {
        return 'row: ' + (n) + ' | ' + 'column: ' + (i + 1);
      });
      d3.selectAll('.square').attr('fill', 'white');
      d3.select(this).attr('fill', '#7AC143');
      d3.select("#imagesvg").append("svg:image").attr("xlink:href", "http://www.iconsdb.com/icons/preview/orange/" + i + "-filled-xxl.png")
                .attr("x", "60")
                .attr("y", "60")
                .attr("width", "90")
                .attr("height", "90")
                .attr("id","image");
    });
    var test = rows.on('mouseleave', function (d, i){
      d3.selectAll("image").remove();
    });
}


}
else if(toggle == 1) {
  toggle = 0;
  d3.select('#grid').style("visibility", "hidden");
  d3.selectAll("#sqauresvg").remove();
  d3.select("#imagesvg").remove();
  console.log("button clicked again");
}

}

function displayimages(){

  if(document.getElementById("place").value == "Bronzeville" || document.getElementById("place").value == "bronzeville"){
    imagefunctionbronzeville();
  }
  else if(document.getElementById("place").value == "Westloop" || document.getElementById("place").value == "westloop"){
    imagefunctionwestloop();
  }
  else if(document.getElementById("place").value == "lakeview" || document.getElementById("place").value == "Lakeview"){
    imagefunctionlakeview();
   }
  else{
    var modal = document.getElementById('searchModal');

// Get the button that opens the modal
var btn = document.getElementById("searchbutton");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function() {
    modal.style.display = "table";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

  }
}
var toggle1998 = 0;
var toggle1990 =0;
var toggle1970 = 0;

function mapload1998(){
  if(toggle1998 == 0){
  var x = document.getElementById("radio1998").value;
  window.initMap();
  }

  }

function mapload1990(){

  var x = document.getElementById("radio1990").value;
  var lakeview = {lat: 41.9415, lng: -87.6622};
  var bronzeville = {lat: 41.8320, lng: -87.6259};
  var westloop = {lat: 41.8825, lng: -87.6447};

  var map = new google.maps.Map(d3.select("#map").node(), {
    zoom: 11,
    center: new google.maps.LatLng(41.88205, -87.627815),
    mapTypeId: google.maps.MapTypeId.TERRAIN,
      styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
            ]
  });



  map.data.loadGeoJson('chicago_communities.geojson');

  map.data.setStyle(function(feature) {
          var color = '#A87916';
          if (feature.getProperty('isColorful')) {
            color = feature.getProperty('color');
          }
          return /** @type {google.maps.Data.StyleOptions} */({
            fillColor: color,
            strokeColor: color,
            strokeWeight: 1
          });
        });

var iconmap1 ={
    url: 'newmarker.png',
    scaledSize: new google.maps.Size(50,50),

  };

 var markerlakeview = new google.maps.Marker({
          position: lakeview,
          map: map,
          icon: iconmap1,
          title: 'Lake View Click to see images'
        });

var markerbronzeville = new google.maps.Marker({
          position: bronzeville,
          map: map,
          icon: iconmap1,
          title: 'Bronzeville Click to see images'
        });

var markerwestloop = new google.maps.Marker({
          position: westloop,
          map: map,
          icon: iconmap1,
          title: 'West Loop Click to see images'
        });

var infowindowlakeview = new google.maps.InfoWindow({
          maxWidth: 200,
          maxHeight: 200,
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1990lakeviewicon.jpg">'
           + '</br>' + '<button id="myBtn" onclick="imagefunctionlakeview1990()">See more images</button>'
        });

var infowindowwestloop = new google.maps.InfoWindow({
    maxWidth: 200,
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1990westloopicon.jpg">' + '</br>'
           + '<button id="myBtn" onclick="imagefunctionwestloop1990()">See more images</button>'
        });

var infowindowbronzeville= new google.maps.InfoWindow({
          maxWidth: 200,
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1990bronzevilleicon.jpg">' + '</br>'
           + '<button id="myBtn" onclick="imagefunctionbronzeville1990()">See more images</button>'
        });

markerlakeview.addListener('click', function() {
        //  map.setZoom(13);
        //  map.setCenter(markerlakeview.getPosition());
          infowindowlakeview.open(map, markerlakeview);
        });

markerwestloop.addListener('click', function() {
        //  map.setZoom(13);
        //  map.setCenter(markerwestloop.getPosition());
          infowindowwestloop.open(map, markerwestloop);
        });

markerbronzeville.addListener('click', function() {
        //  map.setZoom(13);
        //  map.setCenter(markerbronzeville.getPosition());
          infowindowbronzeville.open(map, markerbronzeville);
        });

var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
        //  map.fitBounds(bounds);
        });
var icons = {
          parking: {
            name: 'Field House',
            icon: 'field.png',
            scaledSize: new google.maps.Size(10,10)
          },
          library: {
            name: 'Pool Building',
            icon: 'water.png',
            scaledSize: new google.maps.Size(10,10)
          },
          museum: {
            name: 'Museum',
            icon: 'museum.png',
            scaledSize: new google.maps.Size(10,10)
          },
          info: {
            name: 'Golf Building',
            icon: 'golf.ico',
            scaledSize: new google.maps.Size(10,10)

          }
        };
var legend = document.getElementById('legend');
        for (var key in icons) {
          var type = icons[key];
          var name = type.name;
          var icon = type.icon;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + icon + '"> ' + name;
          legend.appendChild(div);
        }

        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);


  }

function mapload1970(){

  var x = document.getElementById("radio1970").value;
  //alert(x);
  map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 41.89205, lng: -87.777815},
          zoom: 11
        });
  map.data.loadGeoJson('chicago_communities.geojson');

  map.data.setStyle(function(feature) {
          var color = 'gray';
          if (feature.getProperty('isColorful')) {
            color = feature.getProperty('color');
          }
          return /** @type {google.maps.Data.StyleOptions} */({
            fillColor: color,
            strokeColor: color,
            strokeWeight: 1
          });
        });

  var x = document.getElementById("radio1970").value;
  map.data.loadGeoJson('chicago_communities.geojson');
  var myLatLng1 = {lat: 41.9990, lng: -87.8160};
  var fieldhouse ={lat: 41.7817208858308, lng: -87.6048731131776 };
  var myLatLng2 = {lat: 42.00221344869041, lng: -87.68892970173019 };
  var poolbuilding = {lat: 41.70521786388663, lng:  -87.56448542426592 };
  var lakeview = {lat: 41.9415, lng: -87.6622};
  var bronzeville = {lat: 41.8320, lng: -87.6259};
  var westloop = {lat: 41.8825, lng: -87.6447};

var icon ={
    url: "http://www.produtosdelicia.com.br/skin/img/flag.png",
    scaledSize: new google.maps.Size(50,50),

  };

var icon2 ={
    url: "http://www.free-icons-download.net/images/google-maps-icon-66496.png",
    scaledSize: new google.maps.Size(50,50)
  };

var icon3 ={
    url: "http://www.iconsdb.com/icons/preview/royal-blue/water-xxl.png",
    scaledSize: new google.maps.Size(50,50)
  };


  var infowindowfieldhouse1 = new google.maps.InfoWindow({
          content: 'Built in 1970' + '</br>' + '6566 N AVONDALE AVE' + '</br>' + 'OLYMPIA Park'
        })

  var infowindowfieldhouse2 = new google.maps.InfoWindow({
          content: 'Built in 1969' + '</br>' + '6200 S DREXEL AVE' + '</br>' + 'HARRIS (HARRIET) Park'
        })

  var infowindowfieldhouse3 = new google.maps.InfoWindow({
          content: 'Built in 1970' + '</br>' + '6621 N WESTERN AVE' + '</br>' + 'WARREN Park'
        })

  var infowindowpoolhouse = new google.maps.InfoWindow({
          content: 'Built in 1970' + '</br>' + 'Filter Building'
        })

  var infowindowlakeview = new google.maps.InfoWindow({
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1970lakeviewicon.jpg">'
           + '</br>' + '<button id="myBtn" onclick="imagefunctionlakeview1970()">See more images</button>'
        });

  var infowindowwestloop = new google.maps.InfoWindow({
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1970westloopicon.jpg">'
           + '</br>' + '<button id="myBtn" onclick="imagefunctionwestloop1970()">See more images</button>'
        });

  var infowindowbronzeville = new google.maps.InfoWindow({
          content: '<IMG BORDER="0" ALIGN="Left" SRC="1970bronzevilleicon.jpg">'
           + '</br>' + '<button id="myBtn" onclick="imagefunctionbronzeville1970()">See more images</button>'
        });

  var markerfieldhouse1 = new google.maps.Marker({
          position: myLatLng1,
          icon: icon,
          map: map
          });

  var markerfieldhouse2 = new google.maps.Marker({
          position: fieldhouse,
          icon: icon,
          map: map
        });

  var markerfieldhouse3 = new google.maps.Marker({
          position: myLatLng2,
          icon: icon,
          map: map});

  var markerpoolhouse = new google.maps.Marker({
          position: poolbuilding,
          icon: icon3,
          map: map});

  var markerlakeview = new google.maps.Marker({
          position: lakeview,
          icon: icon2,
          map: map});

  var markerbronzeville = new google.maps.Marker({
          position: bronzeville,
          icon: icon2,
          map: map});

var markerwestloop = new google.maps.Marker({
          position: westloop,
          icon: icon2,
          map: map,
          title: 'Hello World!'
        });

   markerlakeview.addListener('click', function() {
    infowindowlakeview.open(map, markerlakeview);
  });

   markerbronzeville.addListener('click', function() {
    infowindowbronzeville.open(map, markerbronzeville);
  });

   markerwestloop.addListener('click', function() {
    infowindowwestloop.open(map, markerwestloop);
  });

  markerfieldhouse1.addListener('click', function() {
    infowindowfieldhouse1.open(map, markerfieldhouse1);
  });

  markerfieldhouse2.addListener('click', function() {
    infowindowfieldhouse2.open(map, markerfieldhouse2);
  });

  markerfieldhouse3.addListener('click', function() {
    infowindowfieldhouse3.open(map, markerfieldhouse3);
  });

markerpoolhouse.addListener('click', function() {
    infowindowpoolhouse.open(map, markerpoolhouse);
  });

}

function imagefunctionwestloop1970(){

  var modal = document.getElementById('myModalwestloop1970');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var imgLOOP1970 = document.getElementById('myImgLOOP1970');
var img1LOOP1970 = document.getElementById('myImg1LOOP1970');
var img2LOOP1970 = document.getElementById('myImg2LOOP1970');
var img3LOOP1970 = document.getElementById('myImg3LOOP1970');
var img4LOOP1970 = document.getElementById('myImg4LOOP1970');
var img5LOOP1970 = document.getElementById('myImg5LOOP1970');
var img6LOOP1970 = document.getElementById('myImg6LOOP1970');
var img7LOOP1970 = document.getElementById('myImg7LOOP1970');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

imgLOOP1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img1LOOP1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img2LOOP1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img3LOOP1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img4LOOP1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img5LOOP1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img6LOOP1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img7LOOP1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}

function imagefunctionwestloop1990(){

  var modal = document.getElementById('myModalwestloop1990');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var imgLOOP1990 = document.getElementById('myImgLOOP1990');
var img1LOOP1990 = document.getElementById('myImg1LOOP1990');
var img2LOOP1990 = document.getElementById('myImg2LOOP1990');
var img3LOOP1990 = document.getElementById('myImg3LOOP1990');
var img4LOOP1990 = document.getElementById('myImg4LOOP1990');
var img5LOOP1990 = document.getElementById('myImg5LOOP1990');
var img6LOOP1990 = document.getElementById('myImg6LOOP1990');
var img7LOOP1990 = document.getElementById('myImg7LOOP1990');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

imgLOOP1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img1LOOP1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img2LOOP1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img3LOOP1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img4LOOP1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img5LOOP1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img6LOOP1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img7LOOP1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}



function imagefunctionlakeview1970(){

  var modal = document.getElementById('myModallakeview1970');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img1970 = document.getElementById('myImg1970');
var img11970 = document.getElementById('myImg11970');
var img21970 = document.getElementById('myImg21970');
var img31970 = document.getElementById('myImg31970');
var img41970 = document.getElementById('myImg41970');
var img51970 = document.getElementById('myImg51970');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

img1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img11970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img21970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img31970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img41970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img51970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}

function imagefunctionlakeview1990(){

  var modal = document.getElementById('myModallakeview1990');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img1990 = document.getElementById('myImg1990');
var img11990 = document.getElementById('myImg11990');
var img21990 = document.getElementById('myImg21990');
var img31990 = document.getElementById('myImg31990');
var img41990 = document.getElementById('myImg41990');
var img51990 = document.getElementById('myImg51990');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

img1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img11990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img21990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img31990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img41990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img51990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}

function imagefunctionbronzeville1970(){

  var modal = document.getElementById('myModalbronzeville1970');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var imgbronze1970 = document.getElementById('myImgbronze1970');
var img1bronze1970 = document.getElementById('myImg1bronze1970');
var img2bronze1970 = document.getElementById('myImg2bronze1970');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

imgbronze1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img1bronze1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img2bronze1970.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}

function imagefunctionbronzeville1990(){

  var modal = document.getElementById('myModalbronzeville1990');
  var span = document.getElementsByClassName("close")[0];
  var btn = document.getElementById("myBtn");
  modal.style.display = "table";

  span.onclick = function() {
    modal.style.display = "none";
}
  window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var imgbronze1990 = document.getElementById('myImgbronze1990');
var img1bronze1990 = document.getElementById('myImg1bronze1990');
var img2bronze1990 = document.getElementById('myImg2bronze1990');

var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption2");

imgbronze1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img1bronze1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
img2bronze1990.onclick = function(){
    modal2.style.display = "table";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal2.style.display = "none";
}
}

function gridfunction(){


  d3.select("#map").style("visibility","hidden");
  d3.select("#gridsection").style("visibility","visible");
  d3.select("#listsection").style("visibility","hidden");
  d3.select("#slideshow").style("visibility","hidden");
  d3.select("#slideshow").style("visibility","hidden");
  d3.select("#searchsection").style("visibility","visible");
  d3.select("#slideshowbridgeport").style("visibility","hidden");
  clearTimeout(myvar);
  clearTimeout(looptime);
  clearTimeout(bridge);
  d3.select("#dots").style("visibility","hidden");
  d3.select("#dotsloop").style("visibility","hidden");
  d3.select("#dotsbridge").style("visibility","hidden");
  d3.select("#slideshowwestloop").style("visibility","hidden");
      // This example uses a GroundOverlay to place an image on the map
      // showing an antique map of Newark, NJ.
 // window.initMap();

}
function neighborhoodfunction(){
  d3.select("#slideshow").style("visibility","hidden");
  d3.select("#searchsection").style("visibility","visible");
  d3.select("#dots").style("visibility","hidden");
  d3.select("#dotsloop").style("visibility","hidden");
  d3.select("#dotsbridge").style("visibility","hidden");
  d3.select("#gridsection").style("visibility","hidden");
  d3.select("#listsection").style("visibility","hidden");
  d3.select("#slideshowbridgeport").style("visibility","hidden");
  d3.select("#map").style("visibility","visible");
  d3.select("#slideshowwestloop").style("visibility","hidden");
  clearTimeout(myvar);
  clearTimeout(looptime);
  clearTimeout(bridge);

}
function historyfunction(){
  d3.select("#gridsection").style("visibility","hidden");
  d3.select("#map").style("visibility","hidden");
  d3.select("#listsection").style("visibility","visible");
  d3.select("#slideshow").style("visibility","hidden");
  d3.select("#searchsection").style("visibility","hidden");
  d3.select("#slideshowbridgeport").style("visibility","hidden");
  d3.select("#slideshow").style("visibility","hidden");
  d3.selectAll("#fade").style("visibility","hidden");
  d3.select("#dots").style("visibility","hidden");
  d3.select("#dotsloop").style("visibility","hidden");
  d3.select("#dotsbridge").style("visibility","hidden");
  d3.select("#slideshowwestloop").style("visibility","hidden");
  clearTimeout(myvar);
  clearTimeout(looptime);
  clearTimeout(bridge);



}
function zoomIn() {
   var e = document.getElementById('mapBGBG');
   var curZoom = document.defaultView.getComputedStyle(e,null).getPropertyValue("zoom")
   console.log(curZoom)
curZoom = Math.abs(parseFloat(curZoom) + .1);
if (curZoom > 1.5)
{
  curZoom = .9;
}


console.log(curZoom)
    document.getElementById('mapBGBG').style.zoom = curZoom

}

function zoomOut()
{
  var e = document.getElementById('mapBGBG');
  var curZoom = document.defaultView.getComputedStyle(e,null).getPropertyValue("zoom")
  console.log(curZoom)
curZoom = Math.abs(parseFloat(curZoom) - .1);

if (curZoom < .5)
{
  curZoom = .9;
}

console.log(curZoom)
   document.getElementById('mapBGBG').style.zoom = curZoom
}


var slideIndex = 0;
var myvar;
function transitionlakeview(){
  d3.select("#gridsection").style("visibility","hidden");
  d3.select("#map").style("visibility","hidden");
  d3.select("#listsection").style("visibility","hidden");
  d3.select("#slideshow").style("visibility","visible");
  d3.select("#dots").style("visibility","visible");
  d3.select("#slideshowwestloop").style("visibility","hidden");
   d3.select("#slideshowbridgeport").style("visibility","hidden");
  d3.selectAll("#fade").style("visibility","visible");
  d3.select("#searchsection").style("visibility","hidden");
  d3.select("#dotsbridge").style("visibility","hidden");
  clearTimeout(looptime);
  clearTimeout(bridge);

  var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex> slides.length) {slideIndex = 1}
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "table";
    dots[slideIndex-1].className += " active";

    myvar=setTimeout(transitionlakeview, 2000); // Change image every 2 seconds
}

var looptime;
function transitionloop(){
  d3.select("#gridsection").style("visibility","hidden");
  d3.select("#map").style("visibility","hidden");
  d3.select("#listsection").style("visibility","hidden");
  d3.select("#slideshow").style("visibility","hidden");
  d3.select("#dots").style("visibility","hidden");
  d3.select("#dotsloop").style("visibility","visible");
  d3.selectAll("#fade").style("visibility","hidden");
  d3.select("#searchsection").style("visibility","hidden");
  d3.select("#slideshowwestloop").style("visibility","visible");
  d3.select("#dotsbridge").style("visibility","hidden");
  d3.select("#slideshowbridgeport").style("visibility","hidden");
  clearTimeout(myvar);
  clearTimeout(bridge);


  var i;
    var slides = document.getElementsByClassName("mySlidesloop");
    var dotsloop = document.getElementsByClassName("dotloop");
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex> slides.length) {slideIndex = 1}
    for (i = 0; i < dotsloop.length; i++) {
        dotsloop[i].className = dotsloop[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "table";
    dotsloop[slideIndex-1].className += " active";
    looptime = setTimeout(transitionloop, 2000); // Change image every 2 seconds
}

var bridge;
function transitionbridge(){
  d3.select("#gridsection").style("visibility","hidden");
  d3.select("#map").style("visibility","hidden");
  d3.select("#listsection").style("visibility","hidden");
  d3.select("#slideshow").style("visibility","hidden");
  d3.select("#dots").style("visibility","hidden");
  d3.selectAll("#fade").style("visibility","hidden");
  d3.select("#searchsection").style("visibility","hidden");
  d3.select("#slideshowwestloop").style("visibility","hidden");
  d3.select("#slideshowbridgeport").style("visibility","visible");
  d3.select("#dotsloop").style("visibility","hidden");
  d3.select("#dotsbridge").style("visibility","visible");
  clearTimeout(myvar);
  clearTimeout(looptime);



  var i;
    var slides = document.getElementsByClassName("mySlidesbridge");
    var dots = document.getElementsByClassName("dotbridge");
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex> slides.length) {slideIndex = 1}
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "table";
    dots[slideIndex-1].className += " active";
    bridge = setTimeout(transitionbridge, 2000); // Change image every 2 seconds
}
