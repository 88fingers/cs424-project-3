//Authors: Laith Sarhan, Sneha Tuteja
//Assignment: CS424 Project 3
//Date: 12/12/2016


Project Report
----------------------
The project report can be found here:
https://bitbucket.org/lsarha2CS441/cs424-project-3/raw/bd11578af12b4f203944a4d80dbb9aaaa5f517af/CS424_Project_3_Report.pdf

Video Overview
----------------------
 The video overview of our project can be found:
 https://youtu.be/mjZ0utayJ8Q

Installation & Running
----------------------
1.	To download our project, please clone from our public bitbucket listed here:
git clone https://lsarha2CS441@bitbucket.org/lsarha2CS441/cs424-project-3.git

2.	To run, simply launch a local server in the root directory of our project and access index.html through a browser ex. http://localhost:8080/index.html

3.	The project is set up in a way that it can be used without any referral API key for setting out the google maps.

4.	For the complete set of images, download the images from the box folder and put all the years in a folder called ‘images’ in the following way:

cs424-project-3 --> images -->1970 (for 1970 images)
cs424-project-3 --> images -->1980 (for 1980 images)
cs424-project-3 --> images -->9899 (for 1998-1999 images)

5.	The path for all these images is already set up.


Datasets Used
----------------------
The data was provided to us by the Special Collections Department of University of Illinois at Chicago.
The data was in the form of thousands of jpg and tif images that were collected by different historical sources and were accumulated by the UIC department.
We were given a set of images for three years: 1970, 1980 and 1998-1999 that were specific to three different neighbourhoods of Chicago city: West loop, Bridgeport-Bronzeville and Lakeview.
Besides that, we were provided a dataset that divides the map of Chicago in equally sized squares and each square represented an aerial image.
We also collected the historical construction dataset that included the new buildings that were being built in our desired years.

Project description and Interaction
----------------------
For the project 3, we worked on Option 3 which was to design a graphical interface to display the historical aerial images of city of Chicago.
The designed interface works with Google earth map to showcase the neighbourhood specific images for all the three years.
The interface is created in a way that it would allow users to preview the images before displaying the full resolution images.

For the second feature, we included a grid view that displays the complete set of images that we had for the city of Chicago. We divided the grid on the basis of the index file that originally divides the map of Chicago on the basis of collected aerial pictures. For a better understanding, we used a detailed Chicago map as a background behind the translucent grid svg that gives the overview of the regions that are represented by the squares.

And finally, as an additional feature of our interactive dashboard, we chose to show the transition of the neighbourhoods that were given to us. We collected the ancient images of those neighbourhoods and created a slideshow to present how these neighbourhoods have emerged throughout this time. We also visualized the construction data in the form of google markers that informs the users about generic details of their construction.

Visualization Tasks
----------------------
•	Compare: Users can compare how different neighbourhoods of Chicagoland have emerged throughout the time.
•	Consume: Users can consume information in terms of the evolution of neighbourhoods as well how each of the showcased neighbourhoods used to look in specific years.
•	Explore/Derive: Explore and derive new information from aerial images.
•	Locate: Locate the aerial photographs of different regions of Chicago on map.
•	Enjoy!

Project Motivations
----------------------
By choosing this project, we thought we could create something like a geospatial interface that would demonstrate the aerial images of Chicago land.
We were fascinated by the kind of dataset we were given and decided to work on it so that we can try and make an interface that would make the aerial images more readily available and easy to interact with.

Audience
----------------------
The audience is not necessarily only Chicago citizens interested in how Chicago as a city has progressed since 1940, but anyone who has a slight interest in viewing the aerial photographs of any particular city. We tried making the interface as intuitive as possible so that it is easier for the users to interact with the interface.

References
----------------------
* http://maps.isgs.illinois.edu/ilhap/
* https://vintageaerial.com/photos/illinois/adams
*https://www.terraserver.com/view?utf8=%E2%9C%93&search_text=Utah%2C+United+States&searchLat=39.515509&searchLng=-111.549668

Work done by each group member
----------------------
Sneha Tuteja: Google map Visualization
Laith Sarhan: Grid Visualization
Sneha Tuteja and Laith Sarhan: Historical slideshow, fixed everything.
